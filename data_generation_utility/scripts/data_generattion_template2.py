""""
This utility helps us in creating training data for CON_SETL_DT.
We have created seprate template for this as it comes in specific format with specific words
Date :- 6th Jan 2020
Author :- Punit Shah
"""

import argparse
import pandas as pd
import utilities
import re

parser = argparse.ArgumentParser()
parser.add_argument("--filter", required=True, help="Kindly type filter to use")
parser.add_argument("--data_file", required=True, help="Kindly pass text file to use for filter values")
args = parser.parse_args()

def genearte_data_single_column(filter_name, data):
    """"
    This function helps creating data for CON_SETL_DT column and it takes txt file with sample values
    This function will create training samples in below formates
    1. trades for <FILTER>
    2. <FIlTER> trades
    """
    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades settled on "
    statement_2 = "trades which are settling on "
    statement_3 = " settled trades"

    filter_values = utilities.read_filter_values(data)

    if filter_name != " " and len(filter_values) !=0:
        for i in filter_values:
            # Create temp Input string with shuffled prefix pattern - 1 and filter values
            _temp_training_ip_statement = str(statement_1 + i)
            # Append it in the final Input list
            training_input_statement.append(_temp_training_ip_statement)
            # Create temp Input string with shuffuled prefix pattern - 2 and filter values
            _temp_training_ip_statement = str(statement_2 + i)
            # Append it in the final Input list
            training_input_statement.append(_temp_training_ip_statement)
            # Create temp Input string with shuffuled prefix pattern - 2 and filter values
            _temp_training_ip_statement = str(i + statement_3)
            # Append it in the final Input list
            training_input_statement.append(_temp_training_ip_statement)
            # Create temp Output string with actual filter name & values for pattern - 1
            _temp_training_op_statement = str(filter_name+" = "+ "\""+i+"\"")
            # Append it in the final Output list
            training_output_statement.append(_temp_training_op_statement)
            # Create temp Output string with actual filter name & values for pattern - 2
            _temp_training_op_statement = str(filter_name + " = "+ "\""+i+"\"")
            # Append it in the final Output list
            training_output_statement.append(_temp_training_op_statement)
            _temp_training_op_statement = str(filter_name + " = "+ "\""+i+"\"")
            # Append it in the final Output list
            training_output_statement.append(_temp_training_op_statement)

    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})

    file_name = "training_data_single_filters"+filter_name+".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)

    print("Success")


def generate_data_single_column_multiple_values(filter_name, data):
    """"
    This function helps creating data for CON_SETL_DT column but with ranges of values and it takes txt file with sample values
    This function will create training samples in below formates
    1. trades for <FILTER>(Range)
    2. <FIlTER>(Range) trades
    """
    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades settling on "
    statement_2 = " settled trades"

    filter_values = utilities.read_filter_values(data)

    if filter_name != " " and len(filter_values) !=0:
        for i in filter_values:
            _temp_str = str(i)
            _temp_training_ip_statement = str(statement_1 + i)
            training_input_statement.append(_temp_training_ip_statement)
            _temp_training_ip_statement = str(i + statement_2)
            training_input_statement.append(_temp_training_ip_statement)
            # Following logic is specific to TRD_DT as it comes in more variety
            if filter_name == 'CON_SETL_DT':
                if re.match(r'.*(between)+.*(and)+|.*(and)+.*(between)+.*',_temp_str):
                    _temp_str = _temp_str.replace("between"," ")
                    _temp_training_op_statement = str(filter_name+" >= "+ "\""+_temp_str.split("and")[0]+"\"" +" AND "+ filter_name+" <= "+ "\""+_temp_str.split("and")[1]+"\"")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)
                elif 'and' in _temp_str:
                    _temp_training_op_statement = str(filter_name+" IN ("+ "\""+_temp_str.split("and")[0]+"\"" +","+ "\""+_temp_str.split("and")[1]+"\")")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)
                else:
                    _temp_training_op_statement = str(filter_name + " >= "+ "\""+i+"\"")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)
            else:
                if "," in _temp_str:
                    _temp_training_op_statement = str(filter_name + " IN (" + "\"" + _temp_str.split(",")[0] + "\"" + "," + "\"" +_temp_str.split(",")[1] + "\")")
                elif "and" in _temp_str:
                    _temp_training_op_statement = str(filter_name + " IN (" + "\"" + _temp_str.split("and")[0] + "\"" + "," + "\"" +_temp_str.split("and")[1] + "\")")
                training_output_statement.append(_temp_training_op_statement)
                training_output_statement.append(_temp_training_op_statement)

    # Create DF for all generated input and output
    single_column_with_multiple_values_file = pd.DataFrame({"Sentence" : training_input_statement,"Query" : training_output_statement})

    # Give sophesticated file name for future reference
    file_name = "training_data_"+filter_name+"_Range.csv"

    # Lets save it
    single_column_with_multiple_values_file.to_csv(PATH +file_name+".csv",
                              index=False)



if __name__ == "__main__":
    genearte_data_single_column(args.filter,args.data_file)