""""
This utility helps us in creating training data for spcified single column.
Date :- 6th Jan 2020
Author :- Punit Shah
"""

import argparse
import pandas as pd
import random
import re
import utilities

parser = argparse.ArgumentParser()
parser.add_argument("--filter", required=True, help="Kindly type filter to use")
parser.add_argument("--data_file", required=True, help="Kindly pass text file to use for filter values")
args = parser.parse_args()


def list_random(ran):
    """
    This function helps in creating more generic training input statements
    """
    random.shuffle(ran)
    return ran[0]


def genearte_data_single_column(filter_name, data):
    """"
    This function helps creating data for single passed column and it takes txt file with sample values
    This function will create training samples in below formates
    1. trades for <FILTER>
    2. <FIlTER> trades
    """
    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " trades"
    #statement_3 = " trades"

    sub_template_1 = [statement_1,statement_2]
    #sub_template_2 = [statement_3]

    filter_values = utilities.read_filter_values(data)

    if filter_name != " " and len(filter_values) !=0:
        for i in filter_values:
            # Shuffle the prefix
            #prefix_1 = list_random(sub_template_1)
            #prefix_2 = list_random(sub_template_2)
            # Create temp Input string with shuffled prefix pattern - 1 and filter values
            _temp_training_ip_statement = str(statement_1 + i)
            # Append it in the final Input list
            training_input_statement.append(_temp_training_ip_statement)
            # Create temp Input string with shuffuled prefix pattern - 2 and filter values
            _temp_training_ip_statement = str(i + statement_2)
            # Append it in the final Input list
            training_input_statement.append(_temp_training_ip_statement)
            # Create temp Output string with actual filter name & values for pattern - 1
            _temp_training_op_statement = str(filter_name+" = "+ "\""+i+"\"")
            # Append it in the final Output list
            training_output_statement.append(_temp_training_op_statement)
            # Create temp Output string with actual filter name & values for pattern - 2
            _temp_training_op_statement = str(filter_name + " = "+ "\""+i+"\"")
            # Append it in the final Output list
            training_output_statement.append(_temp_training_op_statement)

    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})


    file_name = "training_data_single_filters_"+filter_name+".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)
    print("Success")


def generate_data_single_column_multiple_values(filter_name, data):
    """"
    This function helps creating data for single passed column but with ranges of values and it takes txt file with sample values
    This function will create training samples in below formates
    1. trades for <FILTER>(Range)
    2. <FIlTER>(Range) trades
    """
    training_input_statement = []
    training_output_statement = []

    statement_1 = "show me trades for "
    statement_2 = "trades for "
    statement_3 = " trades"

    sub_template_1 = [statement_1,statement_2]
    sub_template_2 = [statement_3]

    filter_values = utilities.read_filter_values(data)

    if filter_name != " " and len(filter_values) !=0:
        for i in filter_values:
            _temp_str = str(i)
            prefix_1 = list_random(sub_template_1)
            prefix_2 = list_random(sub_template_2)
            _temp_training_ip_statement = str(prefix_1 + i)
            training_input_statement.append(_temp_training_ip_statement)
            _temp_training_ip_statement = str(i + prefix_2)
            training_input_statement.append(_temp_training_ip_statement)
            # Following logic is specific to TRD_DT as it comes in more variety
            if filter_name == 'TRD_DT':
                if re.match(r'.*(between)+.*(and)+|.*(and)+.*(between)+.*',_temp_str):
                    _temp_str = _temp_str.replace("between"," ")
                    _temp_training_op_statement = str(filter_name+" >= "+ "\""+_temp_str.split("and")[0]+"\"" +" AND "+ filter_name+" <= "+ "\""+_temp_str.split("and")[1]+"\"")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)
                elif 'and' in _temp_str:
                    _temp_training_op_statement = str(filter_name+" IN ("+ "\""+_temp_str.split("and")[0]+"\"" +","+ "\""+_temp_str.split("and")[1]+"\")")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)
                else:
                    _temp_training_op_statement = str(filter_name + " >= "+ "\""+i+"\"")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)
            else:
                if "," in _temp_str:
                    _temp_training_op_statement = str(filter_name + " IN (" + "\"" + _temp_str.split(",")[0] + "\"" + "," + "\"" +_temp_str.split(",")[1] + "\")")
                elif "and" in _temp_str:
                    _temp_training_op_statement = str(filter_name + " IN (" + "\"" + _temp_str.split("and")[0] + "\"" + "," + "\"" +_temp_str.split("and")[1] + "\")")
                training_output_statement.append(_temp_training_op_statement)
                training_output_statement.append(_temp_training_op_statement)

    single_column_with_multiple_values_file = pd.DataFrame({"Sentence" : training_input_statement,"Query" : training_output_statement})

    file_name = "training_data_"+filter_name+"_Range.csv"
    single_column_with_multiple_values_file.to_csv(PATH +file_name+".csv",
                              index=False)


if __name__ == "__main__":
    cols = ["CLNT_MNEM", "ACCT_NM", "ISIN_CODE", "TRD_DT", "CUSIP_CODE", "NRML_STATUS", "BUY_SELL","QTY_REMN", "FAIL_IND"]
    for i in cols:
        filter = i
        data_file = PATH +i+"_SINGLE.txt"
        genearte_data_single_column(filter,data_file)