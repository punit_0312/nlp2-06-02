""""
This utility helps us in creating training data for two filters at a time with or without RANGE element.
This training data will be specific to CON_SETL_DATE, because CON_SETL_DATE comes in specific format with specific words
Date :- 6th Jan 2020
Author :- Punit Shah
"""

import argparse
import pandas as pd
import utilities
import re
from sklearn.utils import shuffle

parser = argparse.ArgumentParser()
parser.add_argument("--filter_1", required=True, help="Kindly type first filter to use")
parser.add_argument("--filter_2", required=True, help="Kindly type Second filter to use")
parser.add_argument("--filter_3", required=True, help="Kindly type Second filter to use")
parser.add_argument("--data_file_1", required=True, help="Kindly pass text file to use as first filter values")
parser.add_argument("--data_file_2", required=True, help="Kindly pass text file to use as Second filter values")
parser.add_argument("--data_file_3", required=True, help="Kindly pass text file to use as Second filter values")
args = parser.parse_args()


def genearte_data_two_column_with_settlement_date(filter_name1,filter_name2, data1, data2):
    """"
    This function helps creating data for two filters with respect to CON_SETL_DT column and it takes txt file with sample values
    This function will create training samples in below formates
    1. Show me trades for <FILTER> for <CON_SETL_DATE>
    2. <FIlTER> trades settling on <CON_SETL_DATE>
    """
    training_input_statement = []
    training_output_statement = []

    statement_1_part_1 = "trades for "
    statement_1_part_2 = " and settling on "
    statement_2_part_1 = " trades"
    statement_2_part_2 = " settling on "

    filter1_values = utilities.read_filter_values(data1)
    filter2_values = utilities.read_filter_values(data2)

    if filter_name1 and filter_name2 and filter1_values and filter2_values:
        for fv1 in filter1_values:
            for fv2 in filter2_values:
                _temp_training_ip_statement = str(statement_1_part_1 + fv1 + statement_1_part_2 + fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(fv1 + statement_2_part_1 + statement_2_part_2 + fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_op_statement = str(filter_name1+" = "+ "\""+fv1+"\" AND " + filter_name2+" = "+ "\""+fv2+"\"")
                training_output_statement.append(_temp_training_op_statement)
                training_output_statement.append(_temp_training_op_statement)

        file_name = "training_data_two_filters_"+filter_name1 + "_" + filter_name2 +".csv"
        # Creating DF with both lists to make one file
        two_column_file = pd.DataFrame({"Sentence" : training_input_statement, "Query" : training_output_statement})
        two_column_file = shuffle(two_column_file)
        two_column_file.to_csv(PATH +file_name+".csv",index=False)
        print("Success")


def genearte_data_two_column(filter_name1,filter_name2, data1, data2):
    """"
    This function helps creating data for two filters and it takes txt file with sample values
    This function will create training samples in below formates
    1. Show me trades for <FILTER> for <FILTER>
    2. which are the trades for <filter> for <filter>
    """
    training_input_statement = []
    training_output_statement = []

    statement_1_part_1 = "trades for "
    statement_1_part_2 = " with "
    statement_2_part_1 = " trades"
    statement_2_part_2 = " with "
    statement_3_part_1 = " trades"
    statement_3_part_2 = " for "

    filter1_values = utilities.read_filter_values(data1)
    filter2_values = utilities.read_filter_values(data2)

    if filter_name1 and filter_name2 and filter1_values and filter2_values:
        for fv1 in filter1_values:
            for fv2 in filter2_values:
                _temp_training_ip_statement = str(statement_1_part_1 + fv1 + statement_1_part_2 + fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(fv2 + statement_3_part_1 + statement_3_part_2 + fv1)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(fv1 + statement_2_part_1 + statement_2_part_2 + fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_op_statement = str(filter_name1+" = "+ "\""+fv1+"\" AND " + filter_name2+" = "+ "\""+fv2+"\"")
                training_output_statement.append(_temp_training_op_statement)
                training_output_statement.append(_temp_training_op_statement)
                training_output_statement.append(_temp_training_op_statement)

        file_name = "training_data_two_filters_"+filter_name1 + "_" + filter_name2 +".csv"
        # Creating DF with both lists to make one file
        two_column_file = pd.DataFrame({"Sentence" : training_input_statement, "Query" : training_output_statement})
        two_column_file = shuffle(two_column_file)
        two_column_file.to_csv(PATH +file_name+".csv",index=False)
        print("Success")




# def genearte_data_two_column_with_multiple_values(filter_name1,filter_name2, data1, data2):
#     """"
#     This function helps creating data for two filters with multiple values and it takes txt file with sample values
#     This function will create training samples in below formates
#     1. Show me trades for <FILTER> for <FILTER>
#     2. which are the trades for <filter> for <filter>
#     """
#     training_input_statement = []
#     training_output_statement = []
#
#     statement_1_part_1 = "show trades for "
#     statement_1_part_2 = " for "
#     statement_2_part_1 = " trades"
#     statement_2_part_2 = " for "
#
#     filter1_values = utilities.read_filter_values(data1)
#     filter2_values = utilities.read_filter_values(data2)
#
#     if filter_name1 and filter_name2 and filter1_values and filter2_values:
#         for fv1 in filter1_values:
#             for fv2 in filter2_values:
#                 _temp_training_ip_statement = str(statement_1_part_1 + fv1 + statement_1_part_2 + fv2)
#                 training_input_statement.append(_temp_training_ip_statement)
#                 _temp_training_ip_statement = str(fv2 + statement_2_part_1 + statement_2_part_2 + fv1)
#                 training_input_statement.append(_temp_training_ip_statement)
#                 _temp_training_ip_statement = str(statement_1_part_1 + fv2 + statement_1_part_2 + fv1)
#                 training_input_statement.append(_temp_training_ip_statement)
#
#                 if filter_name1 == 'TRD_DT':
#                     if re.match(r'.*(between)+.*(and)+|.*(and)+.*(between)+.*', filter1_values):
#                         _temp_str = _temp_str.replace("between", " ")
#                         _temp_training_op_statement = str(filter_name1 + " >= " + "\"" + _temp_str.split("and")[
#                             0] + "\"" + " AND " + filter_name + " <= " + "\"" + _temp_str.split("and")[1] + "\"")
#                         training_output_statement.append(_temp_training_op_statement)
#                         training_output_statement.append(_temp_training_op_statement)
#                     elif 'and' in _temp_str:
#                         _temp_training_op_statement = str(
#                             filter_name + " IN (" + "\"" + _temp_str.split("and")[0] + "\"" + "," + "\"" +
#                             _temp_str.split("and")[1] + "\")")
#                         training_output_statement.append(_temp_training_op_statement)
#                         training_output_statement.append(_temp_training_op_statement)
#                     else:
#                         _temp_training_op_statement = str(filter_name + " >= " + "\"" + i + "\"")
#                         training_output_statement.append(_temp_training_op_statement)
#                         training_output_statement.append(_temp_training_op_statement)
#                 else:
#                     if "," in _temp_str:
#                         _temp_training_op_statement = str(
#                             filter_name + " IN (" + "\"" + _temp_str.split(",")[0] + "\"" + "," + "\"" +
#                             _temp_str.split(",")[1] + "\")")
#                     elif "and" in _temp_str:
#                         _temp_training_op_statement = str(
#                             filter_name + " IN (" + "\"" + _temp_str.split("and")[0] + "\"" + "," + "\"" +
#                             _temp_str.split("and")[1] + "\")")
#
#                 _temp_training_op_statement = str(filter_name1+" = "+ "\""+fv1+"\" AND " + filter_name2+" = "+ "\""+fv2+"\"")
#                 training_output_statement.append(_temp_training_op_statement)
#                 training_output_statement.append(_temp_training_op_statement)
#                 training_output_statement.append(_temp_training_op_statement)
#
#         file_name = "training_data_two_filters_"+filter_name1 + "_" + filter_name2 +".csv"
#         # Creating DF with both lists to make one file
#         two_column_file = pd.DataFrame({"Sentence" : training_input_statement, "Query" : training_output_statement})
#         two_column_file = shuffle(two_column_file)
#         two_column_file.to_csv("C:\\Users\\punit.shah1\\PycharmProjects\\Rule_Based\\Training_Data\\"+file_name+".csv",index=False)


def genearte_data_multiple_column_with_con_date(filter_name1,filter_name2,filter_name3 ,data1, data2,data3):
    """"
    This function helps creating data for CON_SETL_DT column and it takes txt file with sample values
    This function will create training samples in below formates
    1. trades for <FILTER>
    2. <FIlTER> trades
    """
    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " which are settling on "
    statement_3 = " with  "
    statement_4 = " trades for "
    statement_5 = " which are settling on "

    filter_values1 = utilities.read_filter_values(data1)
    filter_values2 = utilities.read_filter_values(data2)
    filter_values3 = utilities.read_filter_values(data3)


    if filter_name1 and filter_name2 and filter_name3 and data1 and data2 and data3:
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                for fv3 in filter_values3:
                    _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv3 + statement_3 + fv2)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_ip_statement = str(fv2 + statement_4 + fv1 + statement_5 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_op_statement = str(filter_name1 + " = " + "\"" + fv1 +
                                                      "\" AND " + filter_name2 + " = " + "\"" + fv2 +
                                                      "\" AND " + filter_name3 + " = " + "\"" + fv3 + "\"")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)

    file_name = "training_data_three_filters_" + filter_name1 + "_" + filter_name2 + "_" + filter_name3 +".csv"
    # Creating DF with both lists to make one file
    two_column_file = pd.DataFrame({"Sentence": training_input_statement, "Query": training_output_statement})
    two_column_file = shuffle(two_column_file)
    two_column_file.to_csv(PATH + file_name + ".csv",
                           index=False)
    print("Success")


if __name__ == '__main__':
    genearte_data_multiple_column_with_con_date(args.filter_1,
                                                args.filter_2,
                                                args.filter_3,
                                                args.data_file_1,
                                                args.data_file_2,
                                                args.data_file_3)

    #genearte_data_two_column(args.filter_1,args.filter_2,args.data_file_1,args.data_file_2)

