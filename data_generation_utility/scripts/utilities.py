""""
This file defines the logic of multiple components to be used.
Date :- 25-12-2019
Author :- Punit Shah
"""

import os
import glob
import pandas as pd
from sklearn.utils import shuffle

def read_filter_values(data):
    """
    This utility reads filter values and returns it in the form of list
    """
    filter_text_file = open(data, "r")
    lines = filter_text_file.readlines()
    filter_values = []
    for line in lines:
        filter_values.append(line.strip())

    return filter_values


def combine_all_training_data():
    os.chdir(PATH)
    extension = 'csv'
    all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
    # combine all files in the list
    combined_csv = pd.concat([pd.read_csv(f) for f in all_filenames])

    new_combined_csv = shuffle(combined_csv)

    # export to csv
    new_combined_csv.to_csv(FILENAME,
                        index=False,
                        encoding='utf-8-sig')



    print("Success !!!")

if __name__ == "__main__":
    combine_all_training_data()