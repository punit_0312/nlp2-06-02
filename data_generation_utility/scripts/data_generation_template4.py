""""
This utility helps us in creating training data for two filters at a time with or without RANGE element.
This training data will be specific to CON_SETL_DATE, because CON_SETL_DATE comes in specific format with specific words
Date :- 22nd Jan 2020
Author :- Punit Shah
"""

import argparse
import pandas as pd
import utilities
import re
from sklearn.utils import shuffle

parser = argparse.ArgumentParser()
parser.add_argument("--filter_1", required=True, help="Kindly type first filter to use")
parser.add_argument("--filter_2", required=True, help="Kindly type Second filter to use")
parser.add_argument("--filter_3", required=True, help="Kindly type Second filter to use")
parser.add_argument("--data_file_1", required=True, help="Kindly pass text file to use as first filter values")
parser.add_argument("--data_file_2", required=True, help="Kindly pass text file to use as Second filter values")
parser.add_argument("--data_file_3", required=True, help="Kindly pass text file to use as Second filter values")
args = parser.parse_args()

def genearte_data_three_column(filter_name1, filter_name2, filter_name3 , data1, data2, data3):

    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " with "
    statement_3 = " and  "

    filter_values1 = utilities.read_filter_values(data1)
    filter_values2 = utilities.read_filter_values(data2)
    filter_values3 = utilities.read_filter_values(data3)


    if filter_name1 and filter_name2 and filter_name3 and data1 and data2 and data3:
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                for fv3 in filter_values3:
                    _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv2 + statement_3 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_ip_statement = str(fv2 + " " + statement_1 + fv1 + statement_2 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_op_statement = str(filter_name1 + " = " + "\"" + fv1 +
                                                      "\" AND " + filter_name2 + " = " + "\"" + fv2 +
                                                      "\" AND " + filter_name3 + " = " + "\"" + fv3 + "\"")
                    training_output_statement.append(_temp_training_op_statement)
                    training_output_statement.append(_temp_training_op_statement)

    file_name = "training_data_three_filters_" + filter_name1 + "_" + filter_name2 + "_" + filter_name3 +".csv"
    # Creating DF with both lists to make one file
    two_column_file = pd.DataFrame({"Sentence": training_input_statement, "Query": training_output_statement})
    two_column_file = shuffle(two_column_file)
    two_column_file.to_csv(PATH + file_name + ".csv",
                           index=False)
    print("Success")


if __name__ == '__main__':
    genearte_data_three_column(args.filter_1,
                                                args.filter_2,
                                                args.filter_3,
                                                args.data_file_1,
                                                args.data_file_2,
                                                args.data_file_3)

    #genearte_data_two_column(args.filter_1,args.filter_2,args.data_file_1,args.data_file_2)

