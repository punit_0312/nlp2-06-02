import pandas as pd
import random
import utilities

# parser = argparse.ArgumentParser()
# parser.add_argument("--filter_1", required=True, help="Kindly type filter to use")
# parser.add_argument("--filter_2", required=True, help="Kindly type filter to use")
# parser.add_argument("--data_file_1", required=True, help="Kindly pass text file to use for filter values")
# parser.add_argument("--data_file_2", required=True, help="Kindly pass text file to use for filter values")
# args = parser.parse_args()

def list_random(ran):
    """
    This function helps in creating more generic training input statements
    """
    random.shuffle(ran)
    return ran[0]


def genearte_data_three_column_with_settlement_date(filter_1,filter_2,filter_3, data1,data2,data3):

    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " for "
    statement_3 = " which are settling on "
    statement_4 = " trades for "
    statement_5 = " which are settling on "

    filter_values1 = utilities.read_filter_values(data1)
    filter_values2 = utilities.read_filter_values(data2)
    filter_values3 = utilities.read_filter_values(data3)


    if filter_1 != " " and filter_2 != " " and filter_3 != " " and len(filter_values1) !=0 and len(filter_values2) !=0 and len(filter_values3) !=0 :
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                for fv3 in filter_values3:
                    _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv2 + statement_3 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)

                    _temp_training_ip_statement = str(statement_1 + fv2 + statement_2 + fv1 + statement_3 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)

                    _temp_training_ip_statement = str(fv1 + statement_4 + fv2 + statement_5 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)

                    _temp_training_ip_statement = str(fv2 + statement_4 +  fv1 + statement_5 + fv3)
                    training_input_statement.append(_temp_training_ip_statement)

                    # Create temp Output string with actual filter name & values for pattern - 1

                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                      "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                      "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                    training_output_statement.append(_temp_training_op_statement)

                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                      "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                      "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                    training_output_statement.append(_temp_training_op_statement)

                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                      "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                      "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                    training_output_statement.append(_temp_training_op_statement)

                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                      "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                      "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                    training_output_statement.append(_temp_training_op_statement)

    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})

    file_name = "training_data_three_filters_"+ filter_1 + "_" +filter_2 + "_" + filter_3 +".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)
    print("Success")



def genearte_data_three_column_with_cols(filter_1,filter_2,filter_3, data1,data2,data3):

    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " for "
    statement_3 = " with "
    statement_4 = " trades for "
    statement_5 = " with "
    statement_6 = " trades for "
    statement_7 = " for "
    statement_8 = "trades for "
    statement_9 = " which are settling on "
    statement_10 = " with "
    statement_11 = "trades which are settling on "
    statement_12 = " for "
    statement_13 = " with "
    statement_14 = " trades for "
    statement_15 = " which are settling on "

    filter_values1 = utilities.read_filter_values(data1)
    filter_values2 = utilities.read_filter_values(data2)
    filter_values3 = utilities.read_filter_values(data3)


    if filter_1 != " " and filter_2 != " " and filter_3 != " " and len(filter_values1) !=0 and len(filter_values2) !=0 and len(filter_values3) !=0 :
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                for fv3 in filter_values3:
                    if filter_1 == "CON_SETL_DT":
                        _temp_training_ip_statement = str(statement_8 + fv2 + statement_9 + fv1 + statement_10 + fv3)
                        training_input_statement.append(_temp_training_ip_statement)
                        _temp_training_ip_statement = str(statement_11 + fv1 + statement_12 + fv2 + statement_13 + fv3)
                        training_input_statement.append(_temp_training_ip_statement)
                        _temp_training_ip_statement = str(fv3 + statement_14 + fv2 + statement_15 + fv1 )
                        training_input_statement.append(_temp_training_ip_statement)

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)
                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)
                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)
                    else:
                        _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv2 + statement_3 + fv3)
                        training_input_statement.append(_temp_training_ip_statement)

                        _temp_training_ip_statement = str(statement_1 + fv2 + statement_2 + fv1 + statement_3 + fv3)
                        training_input_statement.append(_temp_training_ip_statement)

                        _temp_training_ip_statement = str(fv1 + statement_4 + fv2 + statement_5 + fv3)
                        training_input_statement.append(_temp_training_ip_statement)

                        _temp_training_ip_statement = str(fv2 + statement_4 +  fv1 + statement_5 + fv3)
                        training_input_statement.append(_temp_training_ip_statement)

                        _temp_training_ip_statement = str(fv3 + statement_6 + fv1 + statement_7 + fv2)
                        training_input_statement.append(_temp_training_ip_statement)

                        _temp_training_ip_statement = str(fv3 + statement_6 +  fv2 + statement_7 + fv1)
                        training_input_statement.append(_temp_training_ip_statement)

                        # Create temp Output string with actual filter name & values for pattern - 1

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)

                        _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 +
                                                          "\" AND " + filter_2 + " = " + "\"" + fv2 +
                                                          "\" AND " + filter_3 + " = " + "\"" + fv3 + "\"")
                        training_output_statement.append(_temp_training_op_statement)

    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})

    file_name = "training_data_three_filters_"+ filter_1 + "_" +filter_2 + "_" + filter_3 +".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)
    print("Success")



if __name__ == "__main__":
    cols_1 = ["TRD_DT", "NRML_STATUS", "BUY_SELL", "FAIL_IND","CON_SETL_DT"]
    cols_2 = ["CLNT_MNEM", "ACCT_NM", "ISIN_CODE", "CUSIP_CODE", "BUY_SELL", "FAIL_IND"]
    with_cols = ["NRML_STATUS","QTY_REMN"]

    for i in cols_1:
        for j in cols_2:
            for k in with_cols:
                filter_1 = i
                filter_2 = j
                filter_3 = k
                data_file_1 = PATH +i+"_SINGLE.txt"
                data_file_2 = PATH +j+"_SINGLE.txt"
                data_file_3 = PATH +k+"_SINGLE.txt"
                if filter_1 != filter_2 and filter_1 != filter_3 and filter_2 != filter_3:
                    genearte_data_three_column_with_cols(filter_1,filter_2,filter_3,data_file_1,data_file_2,data_file_3)
