import pandas as pd
import random
import utilities

# parser = argparse.ArgumentParser()
# parser.add_argument("--filter_1", required=True, help="Kindly type filter to use")
# parser.add_argument("--filter_2", required=True, help="Kindly type filter to use")
# parser.add_argument("--data_file_1", required=True, help="Kindly pass text file to use for filter values")
# parser.add_argument("--data_file_2", required=True, help="Kindly pass text file to use for filter values")
# args = parser.parse_args()

def list_random(ran):
    """
    This function helps in creating more generic training input statements
    """
    random.shuffle(ran)
    return ran[0]


def genearte_data_two_column(filter_1,filter_2,data_1,data_2):

    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " for "
    statement_3 = " trades for "

    filter_values1 = utilities.read_filter_values(data_1)
    filter_values2 = utilities.read_filter_values(data_2)

    if filter_1 != " " and filter_2 != " " and len(filter_values1) !=0 and len(filter_values2) !=0:
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(statement_1 + fv2 + statement_2 + fv1)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(fv1 + statement_3 +  fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(fv2 + statement_3 +  fv1)
                training_input_statement.append(_temp_training_ip_statement)
                # Create temp Output string with actual filter name & values for pattern - 1
                _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                training_output_statement.append(_temp_training_op_statement)
                _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                training_output_statement.append(_temp_training_op_statement)
                _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                training_output_statement.append(_temp_training_op_statement)
                _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                training_output_statement.append(_temp_training_op_statement)

    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})

    file_name = "training_data_two_filters_"+ filter_1 + "_" +filter_2+".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)
    print("Success")




def genearte_data_two_column_with_settlement_date(filter_1,filter_2,data_1,data_2):

    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " and settling on "
    statement_3 = " trades settling on "

    filter_values1 = utilities.read_filter_values(data_1)
    filter_values2 = utilities.read_filter_values(data_2)

    if filter_1 != " " and filter_2 != " " and len(filter_values1) !=0 and len(filter_values2) !=0:
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv2)
                training_input_statement.append(_temp_training_ip_statement)
                _temp_training_ip_statement = str(fv1 + statement_3 +  fv2)
                training_input_statement.append(_temp_training_ip_statement)

                # Create temp Output string with actual filter name & values for pattern - 1
                _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                training_output_statement.append(_temp_training_op_statement)
                _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                training_output_statement.append(_temp_training_op_statement)


    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})

    file_name = "training_data_two_filters_"+ filter_1 + "_" +filter_2+".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)
    print("Success")



def genearte_data_two_column_with_columns(filter_1,filter_2,data_1,data_2):

    training_input_statement = []
    training_output_statement = []

    statement_1 = "trades for "
    statement_2 = " with "
    statement_3 = " trades with "
    statement_4 = "trades with "
    statement_5 = " and settling on "
    statement_6 = "trades settling on "
    statement_7 = " with "
    statement_8 = " trades for "

    filter_values1 = utilities.read_filter_values(data_1)
    filter_values2 = utilities.read_filter_values(data_2)

    if filter_1 != " " and filter_2 != " " and len(filter_values1) !=0 and len(filter_values2) !=0:
        for fv1 in filter_values1:
            for fv2 in filter_values2:
                if filter_1 == 'CON_SETL_DT':
                    _temp_training_ip_statement = str(statement_4 + fv2 + statement_5 + fv1)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_ip_statement = str(statement_6 + fv1 + statement_7 + fv2)
                    training_input_statement.append(_temp_training_ip_statement)

                    _temp_training_op_statement = str(
                        filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 + "\"")
                    training_output_statement.append(_temp_training_op_statement)
                    _temp_training_op_statement = str(
                        filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 + "\"")
                    training_output_statement.append(_temp_training_op_statement)
                else:
                    _temp_training_ip_statement = str(statement_1 + fv1 + statement_2 + fv2)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_ip_statement = str(fv2 + statement_8 + fv1)
                    training_input_statement.append(_temp_training_ip_statement)
                    _temp_training_ip_statement = str(fv1 + statement_3 +  fv2)
                    training_input_statement.append(_temp_training_ip_statement)

                    # Create temp Output string with actual filter name & values for pattern - 1
                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                    training_output_statement.append(_temp_training_op_statement)
                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                    training_output_statement.append(_temp_training_op_statement)
                    _temp_training_op_statement = str(filter_1 + " = " + "\"" + fv1 + "\"" + "AND " + filter_2 + " = " + "\"" + fv2 +"\"")
                    training_output_statement.append(_temp_training_op_statement)

    # Creating DF with both lists to make one file
    single_column_file = pd.DataFrame({"Sentence" : training_input_statement,
                                       "Query" : training_output_statement})

    file_name = "training_data_two_filters_"+ filter_1 + "_" +filter_2+".csv"
    single_column_file.to_csv(PATH +file_name+".csv",
                              index=False)
    print("Success")




if __name__ == "__main__":
    cols = ["TRD_DT","CLNT_MNEM", "ACCT_NM", "ISIN_CODE", "CUSIP_CODE", "NRML_STATUS", "BUY_SELL", "FAIL_IND","CON_SETL_DT"]
    with_cols = ["NRML_STATUS","QTY_REMN"]
    for i in cols:
        for j in with_cols:
            filter_1 = i
            filter_2 = j
            data_file_1 = PATH +i+"_SINGLE.txt"
            data_file_2 = PATH +j+"_SINGLE.txt"
            #genearte_data_two_column(filter_1,filter_2,data_file_1,data_file_2)
            #genearte_data_two_column_with_settlement_date(filter_1,filter_2,data_file_1,data_file_2)
            if filter_1 != filter_2:
                genearte_data_two_column_with_columns(filter_1,filter_2,data_file_1,data_file_2)
