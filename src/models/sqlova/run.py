import time
import preprocessing
import config
import regex_pre_processing
import add_question
import util_preprocessing
import re

from class_output_mapper import Output_Mapper
from class_date_processing import DateProcess
from annotate_ws import Annotate_WS
from intent_classification.multi_intent_citi import Multi_Intent
from predict import Predict
from graph import neo4j_current
from datetime import datetime

obj_annotate=Annotate_WS()
obj_predict=Predict()
obj_Intent=Multi_Intent()
dt_parser_obj = DateProcess()
ot_mapper_obj = Output_Mapper()


# Calls pre-processing function
def run_pre_processing(query):
    updated_query,extracted_date = preprocessing.run(query)
    return updated_query,extracted_date


def run_sqlova(question,table_id,request_id=0):

    # Lets get the current timestamp which acts as unique request number for each request
    # We shall use this request_id as split to create and utilize JSONL files which will be unique for each requests.
    request_id = int(time.time())
    add_question.question_to_json(str(request_id),str(table_id),str(question))
    obj_annotate.main(str(request_id))
    _temp_result = obj_predict.predict_run(split=str(request_id))
    try:
        # Returns only where part of sql
        result = str(_temp_result[0]['sql']).split("WHERE")[1]

    except:
        # if sqlova output is empty then returns the same
        result = 'empty'
    return result


def sqlova_output_processing(sqlova_output,master_dict):

    _temp_outout_str = str(sqlova_output)

    # Check if we have got something in sqlova or not
    if sqlova_output != 'empty':
        if "AND" in _temp_outout_str:
            _temp_list = _temp_outout_str.split("AND")
            if len(_temp_list) > 1:
                for i in range(0,len(_temp_list)):
                    ch = ''
                    if '=' in str(_temp_list[i]):
                        ch = '='
                    elif '<' in str(_temp_list[i]):
                        ch = '<'
                    else:
                        ch = '>'
                    _temp_list_element = str(_temp_list[i]).split(ch)
                    key_element = str(_temp_list_element[0]).strip()
                    value_element = str(_temp_list_element[1]).strip()
                    if str(key_element) in config.master_mapper_dict:
                        master_dict[config.master_mapper_dict.get(key_element,None)].append(ch + " " + value_element)
        else:
            ch = ''
            if '=' in _temp_outout_str:
                ch = '='
            elif '<' in _temp_outout_str:
                ch = '<'
            else:
                ch = '>'
            _temp_list = _temp_outout_str.split(ch)
            key_element = str(_temp_list[0]).strip()
            value_element = str(_temp_list[1]).strip()
            if str(key_element) in config.master_mapper_dict:
                master_dict[config.master_mapper_dict.get(key_element, None)].append(ch + " " + value_element)
    else:
        # If sqlova output is blank than return master dict as is.
        pass
    return master_dict


def intent_check(pre_procssed_query):

    # Takes pre-processed query and writes prediction backs to disk
    # We are using request_id to create file name as it will be presisted across multiple sessions
    extracted_intent=obj_Intent.run(str(pre_procssed_query))
    return extracted_intent


def validation(master_dict,query):

    # Check the length of extracted value and discard unnessary
    for key,value in master_dict.items():

        if value:
            temp_value_str = " ".join(value)
            if len(temp_value_str) < 3:
                master_dict[key] = ''
            if key in config.date_column_list:
                date = " ".join(master_dict.get(key)).replace("= ","").replace("< ","").replace("> ","")
                result = re.match(r"(\d\d\d\d-\d\d-\d\d(?:\sand|\sor))", date)
                result1 = re.match(r"^(\d\d\d\d-\d\d-\d\d)$",date)
                if result or result1:
                    pass
                else:
                    master_dict[key] = ''

            if key == 'NRML_STATUS':
                unwanted_word = 'status'
                temp_status = [tv.replace(unwanted_word,"") for tv in master_dict.get(key)]
                temp_status = " ".join(temp_status).replace("= ","")
                if temp_status in util_preprocessing.nrml_status or temp_status == 'settled':
                    pass
                else:
                    master_dict[key] = ''

            if key == 'FAIL_IND':
                if master_dict.get(key) in util_preprocessing.fail_indicator_values:
                    pass
                else:
                    master_dict[key] = ''

            if key == 'BUY_SELL':
                if master_dict.get(key) in util_preprocessing.buy_sell:
                    pass
                else:
                    master_dict[key] = ''

            if key == 'CLNT_MNEM':
                unwanted_word = ' client'
                temp_client_value = [tv.replace(unwanted_word, "") for tv in master_dict.get(key)]
                if temp_client_value in regex_pre_processing.client_neumonics:
                    pass
                else:
                    master_dict[key] = ''

            if key in config.quantitative_column_list:
                post_processing(key,master_dict,query)

    return master_dict


def graph_processing(master_dict,predicted_intent,query,extracted_date):

    extracted_intent_list = [key for key,value in master_dict.items() if value]

    if len(extracted_intent_list) < 1:
        graph_results = neo4j_current.getCol(predicted_intent,query,master_dict,extracted_date)
    else:
        predicted_intent.sort()
        extracted_intent_list.sort()
        if len(predicted_intent) == len(extracted_intent_list):
            matched_intents = list(set(predicted_intent).intersection(set(extracted_intent_list)))
            matched_intents.sort()

            if matched_intents == extracted_intent_list:
                return master_dict
            else:
                missing_intents = set(predicted_intent).symmetric_difference(set(extracted_intent_list))
                graph_results = neo4j_current.getCol(missing_intents, query,master_dict,extracted_date)
        else:
            missing_intents = set(predicted_intent).symmetric_difference(set(extracted_intent_list))
            graph_results = neo4j_current.getCol(missing_intents,query,master_dict,extracted_date)
    if graph_results:
        master_dict.update(graph_results)
    else:
        pass
    return master_dict


def post_processing(key, master_dict,query):
    """
    This function will take care of quantaity handling
    """

    temp_val_str = "".join(master_dict.get(key))
    
    try:
        if "= " in temp_val_str:
            ch = "= "
        elif "> " in temp_val_str or "more" in query or "greater" in query:
            ch = "> "
        elif "<" in temp_val_str or "less" in query:
            ch = "< "
    except:
        ch = 999.00

    query_list  = query.split(" ")
    multi_value_list = []

    parsed_output = util_preprocessing.duckling_wrapper.parse_number(query)

    if parsed_output:
        if len(parsed_output) > 1:
            for i in range(0,len(parsed_output)):
                text = parsed_output[i]['text']
                value = parsed_output[i]['value']['value']
                for w in query_list:
                    if text == w:
                        multi_value_list.append(str(value))
                    else:
                        pass
            if len(multi_value_list) > 1:
                master_dict[key] = [" and ".join(multi_value_list)]
            elif len(multi_value_list) == 1:
                master_dict[key] = [ch + multi_value_list[0]]
            else:
                master_dict[key] = ''
        else:
            text = parsed_output[0]['text']
            value = parsed_output[0]['value']['value']
            if any(text == w for w in query_list):
                master_dict[key] = [ch + str(value)]
            else:
                master_dict[key] = '' 
    else:
        master_dict[key] = ''



# This is the pivotal function which starts execution sequentially
def run(query, master_preprocessing_dict,conversation_id,user_id):

    # Below line executes regex processing and returns updated query with master dict which has values appened to appropriate keys
    regex_processed_query, intent_query, master_directory = regex_pre_processing.run(query, master_preprocessing_dict)

    # Below line executes pre-processing steps and returns updated query
    pre_processed_query, extracted_date = run_pre_processing(regex_processed_query)

    # Below line executes sqlova code which takes pre-processed query
    sqlova_output = run_sqlova(pre_processed_query,config.table_id)
    # Lets write the sqlova output
    with open(config.sqlova_result_file_name, 'a') as file:
        file.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " -- " + conversation_id + " -- " + user_id + " -- " + sqlova_output +"\n")

    # Below line takes sqlova output and transforms into required format
    sqlova_processed_dict = sqlova_output_processing(sqlova_output,master_directory)

    # Below line executes intent classification and predicts the intents. it writes predicted output to config specified location
    predicted_intent = intent_check(intent_query.lower())
    # Write the result of intent classification
    with open(config.intent_result_file_name, 'a') as file:
        file.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " -- " + conversation_id + " -- " + user_id + " -- " + ",".join(predicted_intent) +"\n")

    # Below line checks if extracted intents has expected value or not and accrodingly takes actions
    validated_dict = validation(sqlova_processed_dict,regex_processed_query)

    # Below line calls graph with validated dict
    graph_processed_dict = graph_processing(validated_dict,predicted_intent,regex_processed_query,extracted_date)

    result_list = ot_mapper_obj.mapper(graph_processed_dict)
    return result_list