
import argparse, os
from sqlnet.dbengine import DBEngine
from sqlova.utils.utils_wikisql import *
from train import construct_hyper_param, get_models
import config
import torch
model = model_bert = tokenizer = bert_config = args = None

class Predict():
    def __init__(self):
        """
        Loading Model
        """
        self.get_model_new()

    def evalution(self,results):
        return results

    # This is a stripped down version of the test() method in train.py - identical, except:
    #   - does not attempt to measure accuracy and indeed does not expect the data to be labelled.
    #   - saves plain text sql queries.
    #
    def predict(self,data_loader, data_table, model, model_bert, bert_config, tokenizer,
                max_seq_length,
                num_target_layers, detail=False, st_pos=0, cnt_tot=1, EG=False, beam_size=4,
                path_db=None, dset_name='test'):

        model.eval()
        model_bert.eval()

        engine = DBEngine(os.path.join(path_db, f"{dset_name}.db"))
        results = []
        for iB, t in enumerate(data_loader):
            nlu, nlu_t, sql_i, sql_q, sql_t, tb, hs_t, hds = get_fields(t, data_table, no_hs_t=True, no_sql_t=True)
            g_sc, g_sa, g_wn, g_wc, g_wo, g_wv = get_g(sql_i)
            g_wvi_corenlp = get_g_wvi_corenlp(t)
            wemb_n, wemb_h, l_n, l_hpu, l_hs, \
            nlu_tt, t_to_tt_idx, tt_to_t_idx \
                = get_wemb_bert(bert_config, model_bert, tokenizer, nlu_t, hds, max_seq_length,
                                num_out_layers_n=num_target_layers, num_out_layers_h=num_target_layers)
            if not EG:
                # No Execution guided decoding
                s_sc, s_sa, s_wn, s_wc, s_wo, s_wv = model(wemb_n, l_n, wemb_h, l_hpu, l_hs)
                pr_sc, pr_sa, pr_wn, pr_wc, pr_wo, pr_wvi = pred_sw_se(s_sc, s_sa, s_wn, s_wc, s_wo, s_wv, )
                pr_wv_str, pr_wv_str_wp = convert_pr_wvi_to_string(pr_wvi, nlu_t, nlu_tt, tt_to_t_idx, nlu)
                pr_sql_i = generate_sql_i(pr_sc, pr_sa, pr_wn, pr_wc, pr_wo, pr_wv_str, nlu)
            else:
                # Execution guided decoding
                prob_sca, prob_w, prob_wn_w, pr_sc, pr_sa, pr_wn, pr_sql_i = model.beam_forward(wemb_n, l_n, wemb_h,
                                                                                                l_hpu,
                                                                                                l_hs, engine, tb,
                                                                                                nlu_t, nlu_tt,
                                                                                                tt_to_t_idx, nlu,
                                                                                                beam_size=beam_size)
                # sort and generate
                pr_wc, pr_wo, pr_wv, pr_sql_i = sort_and_generate_pr_w(pr_sql_i)
                # Following variables are just for consistency with no-EG case.
                pr_wvi = None  # not used
                pr_wv_str = None
                pr_wv_str_wp = None

            pr_sql_q = generate_sql_q(pr_sql_i, tb)

            for b, (pr_sql_i1, pr_sql_q1) in enumerate(zip(pr_sql_i, pr_sql_q)):
                results1 = {}
                results1["query"] = pr_sql_i1
                results1["table_id"] = tb[b]["id"]
                results1["nlu"] = nlu[b]
                results1["sql"] = pr_sql_q1
                results.append(results1)
        return results

    ## Set up hyper parameters and paths
    def predict_run(self,split):

        # Load data
        dev_data, dev_table = load_wikisql_data(args.data_path, mode=split, toy_model=args.toy_model,
                                                toy_size=args.toy_size, no_hs_tok=True)

        dev_loader = torch.utils.data.DataLoader(
            batch_size=args.bS,
            dataset=dev_data,
            shuffle=False,
            num_workers=0,
            collate_fn=lambda x: x  # now dictionary values are not merged!
        )


        with torch.no_grad():
            results = self.predict(dev_loader,
                              dev_table,
                              model,
                              model_bert,
                              bert_config,
                              tokenizer,
                              args.max_seq_length,
                              args.num_target_layers,
                              detail=False,
                              path_db=args.data_path,
                              st_pos=0,
                              dset_name=split, EG=args.EG)
        return results

    def get_model_new(self):

        global model, model_bert, tokenizer, bert_config, args

        parser = argparse.ArgumentParser()

        parser.add_argument("--model_file", default=config.model_path, help='model file to use (e.g. model_best.pt)')
        parser.add_argument("--bert_model_file", default=config.model_bert_path,
                            help='bert model file to use (e.g. model_bert_best.pt)')
        parser.add_argument("--bert_path", default=config.bert_config_path,
                            help='path to bert files (bert_config*.json etc)')
        parser.add_argument("--data_path", default=config.IN_support_directoy, help='path to *.jsonl and *.db files')
        parser.add_argument("--split", default='citi', help='prefix of jsonl and db files (e.g. dev)')
        parser.add_argument("--result_path", help='directory in which to place results')
        args = construct_hyper_param(parser)

        BERT_PT_PATH = args.bert_path
        # path_save_for_evaluation = args.result_path

        # Load pre-trained models
        path_model_bert = args.bert_model_file
        path_model = args.model_file
        args.no_pretraining = True  # counterintuitive, but avoids loading unused models
        model, model_bert, tokenizer, bert_config = get_models(args, BERT_PT_PATH, trained=True, path_model_bert=path_model_bert, path_model=path_model)