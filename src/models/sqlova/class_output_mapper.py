import config

class Output_Mapper():


    def single_value_extraction_and_mapping(self,key,value):

        temp_val_str = " ".join(value).upper()
        if "= " in temp_val_str:
            ch = "= "
            temp_val_str = temp_val_str.replace(ch, "")
            # Check if we have multiple values from sqlova output such as two account names or two dates
            if " AND " in temp_val_str:
                temp_val_list = temp_val_str.split("AND")
                # If we have got 2 values in date columns then we will use BETWEEN condition
                if key in config.date_column_list or key in config.quantitative_column_list:
                    temp_json = {'id': key, 'condition': 'between', 'value': [tv.strip() for tv in temp_val_list]}
                else:
                    # If we got 2 values in any other columns we will use EQUAL condition
                    temp_json = {'id': key, 'condition': 'equals', 'value': [tv.strip().upper() for tv in temp_val_list]}
            elif " OR " in temp_val_str:
                temp_val_list = temp_val_str.split("OR")
                if key in config.date_column_list or key in config.quantitative_column_list:
                    temp_json = {'id': key, 'condition': 'between', 'value': [tv.strip() for tv in temp_val_list]}
                else:
                    pass            
            else:
                temp_json = {'id': key, 'condition': 'equals', 'value': [temp_val_str.strip()]}
        elif "> " in temp_val_str:
            ch = "> "
            temp_val_str = temp_val_str.replace(ch, "")
            temp_json = {'id': key, 'condition': 'greater', 'value': [temp_val_str.strip()]}
        elif "< " in temp_val_str :
            ch = "< "
            temp_val_str = temp_val_str.replace(ch, "")
            temp_json = {'id': key, 'condition': 'lesser', 'value': [temp_val_str.strip()]}
        else:
            if " AND " in temp_val_str:
                temp_val_list = temp_val_str.split("AND")
                temp_json = {'id': key, 'condition': 'between', 'value': [tv.strip() for tv in temp_val_list]}
            else:
                temp_json = {'id': key, 'condition': 'equals', 'value': [temp_val_str.strip()]}
        return temp_json

    def multiple_value_extraction_and_mapping(self,key,value):
        temp_value = " ".join(value).upper().split(" ")
        temp_json = {'id': key, 'condition': 'equals', 'value': temp_value}
        return temp_json

    def mapper(self,master_dict):

        resultant_list = []
        resultant_json = {"recipient_id": "default",
                          "text": []}

        for key, value in master_dict.items():
            if value and len(value) == 1:
                try:
                    internal_json = self.single_value_extraction_and_mapping(key,value)
                    try:
                        resultant_json['text'].append(internal_json)
                    except:
                        ValueError("Internal JSON couldn't be appended to final list in Output Formatter")
                except:
                    ValueError("Internal JSON is not set in Output Formatter")

            if value and len(value) > 1:
                internal_json = self.multiple_value_extraction_and_mapping(key,value)
                try:
                    resultant_json['text'].append(internal_json)
                except:
                    ValueError("Internal JSON couldn't be appended to final list in Output Formatter")

        resultant_list.append(resultant_json)
        return resultant_list
