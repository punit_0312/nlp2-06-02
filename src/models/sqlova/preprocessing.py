"""
This module implements the preprocessing for SQLova
This handles following things
1. Length Check
2. Unwanted Word/Character removal
3. Tagging of dates from english words i.e. Today, tomorrow, yesterday
Author :- Punit Shah
Date :- 09th Jan 2020
"""

# Loading required packages
import re
from class_date_processing import DateProcess
dt_parser_obj = DateProcess()


def handle_settled_flag(query):
    """
    This function specifically handles status settled as this overlaps with settlement date
    """
    if re.match(r"(\d\d\d\d-\d\d-\d\d)",query):
        return query
    else:
        query = query.replace("settled", "settled status")
        return query


def run(query):

    # parse dates using Duckling
    date_query, extracted_date = dt_parser_obj.dates_processing(query)
    status_updated_query = handle_settled_flag(date_query)
    # returns updated query
    return status_updated_query,extracted_date