import numpy as np
import keras
import tensorflow as tf
import tensorflow_hub as hub
import config
from keras.models import Model
from keras.layers import Input
from keras.layers.core import Dense, Lambda
import os
from keras import backend as K
from tensorflow import Graph,Session

class Query_Classification():

    def __init__(self):
        self.model = self.build_model()

    def ELMoEmbedding(self,x):
        embed = hub.Module("query_classification\\9bb74bc86f9caffc8c47dd7b33ec4bb354d9602d")
        # return embed(tf.squeeze(tf.cast(x, tf.string)), signature="default", as_dict=True)["default"]
        return embed(tf.reshape(tf.cast(x, tf.string), [-1]), signature="default", as_dict=True)["default"]

    def build_model(self):
        
        global graph1
        global session1
        with tf.Graph().as_default() as graph1:
            session1 = Session()
            with session1.as_default():
                input_text = Input(shape=(1,), dtype="string")
                embedding = Lambda(self.ELMoEmbedding, output_shape=(1024,),)(input_text)
                dense3 = Dense(256, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001))(embedding)
                dense4 = Dense(128, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001))(dense3)
                pred = Dense(1, activation='sigmoid')(dense4)
                model = Model(inputs=[input_text], outputs=pred)
                model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
                model.load_weights("query_classification\\full_model_elmo_weights_query_classification.h5")
                return model

    def pred(self,arr):

        with graph1.as_default():
            with session1.as_default():
                preds = self.model.predict(arr)
       
        if preds <= 0.50:
            return 0
        else:
            return 1

    def run(self,query):
        input_str = query
        input_arr = np.array([input_str])
        extracted_intents = self.pred(input_arr)
        return extracted_intents


