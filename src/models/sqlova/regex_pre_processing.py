# Loading required packages
import util_preprocessing
import re

client_neumonics = []
with open("data//CLNT_MNEM.txt") as file:
    for line in file:
        client_neumonics.append(line.lower().strip())

# ISIN_Code procssing
def isin_check(query,master_pp_dict):
    """
    This function checkes if user query contains anywhich which looks like pre-defined syntex of ISIN_Code.
    If the pattern matches then it will be taken out
    This function takes user query and returns ISIN_element and user query.
    """

    isin_code_check = re.findall(util_preprocessing.isin_code_regex, query, re.IGNORECASE)

    if isin_code_check:
        _temp_user_query = query
        _temp_intent_query = query
        if len(isin_code_check) > 1:
            for ic in isin_code_check:
                _temp_user_query = _temp_user_query.replace(ic,'')
                _temp_intent_query = _temp_intent_query.replace(ic,str(ic + " " +util_preprocessing.isin_suffix))
                master_pp_dict['ISIN_CODE'].append(ic)
        else:
            _temp_user_query = _temp_user_query.replace(''.join(isin_code_check),'')
            _temp_intent_query = _temp_intent_query.replace(''.join(isin_code_check), str(''.join(isin_code_check) + " " +util_preprocessing.isin_suffix))
            master_pp_dict['ISIN_CODE'].append(isin_code_check[0])
        return _temp_user_query, _temp_intent_query, master_pp_dict
    else:
        return query, query, master_pp_dict



# cusip processing
def cusip_check(query, intent_check_query, master_pp_dict):
    """
    This function checkes if user query contains anywhich which looks like pre-defined syntex of cusip_code.
    If the pattern matches then it will be taken out
    This function takes user query with extacted elements appended with pre-defined word.
    """

    cusip_code_check = re.findall(util_preprocessing.cusip_code_regex, query, re.IGNORECASE)

    if cusip_code_check:
        _temp_user_query = query
        _temp_intent_query = query
        if len(cusip_code_check) > 1:
            for cc in cusip_code_check:
                _temp_user_query = _temp_user_query.replace(cc,'')
                _temp_intent_query = _temp_intent_query.replace(cc,str(cc + " " + util_preprocessing.cusip_suffix))
                master_pp_dict['CUSIP_CODE'].append(cc)
        else:
            _temp_user_query = _temp_user_query.replace(''.join(cusip_code_check),'')
            _temp_intent_query = _temp_intent_query.replace(''.join(cusip_code_check), str(''.join(cusip_code_check) + " " + util_preprocessing.cusip_suffix))
            master_pp_dict['CUSIP_CODE'].append(cusip_code_check[0])
        return _temp_user_query, _temp_intent_query, master_pp_dict
    else:
        return query, intent_check_query, master_pp_dict



# flag processing
def mark_flags(query):
    """
    This function checkes if user query contains pre-defined flags for buy or normal status.
    If the pattern matches then it will be taken out and appened with prefix for SQLova
    """
    word_list = str(query).split(" ")
    for index,w in enumerate(word_list):
        if w in client_neumonics:
            word_list[index] = str(w + " client")
        if w in util_preprocessing.buy_sell:
            word_list[index] = str(w + util_preprocessing.buy_sell_suffix)
        if w in util_preprocessing.nrml_status:
            word_list[index] = str(w + util_preprocessing.nrml_status_suffix)
        if w in util_preprocessing.fail_status:
            word_list[index] = str(w + util_preprocessing.fail_status_suffix)
    query = " ".join(word_list)
    return query,query



def validate_query(query):

    query = query.lower()
    query = query.replace("'", "")
    query = query.replace(","," and ")

    if len(query) >= 9:
        return query
    else:
        raise Exception("Query doesn't satisfy mandatory conditions. Kindly enter valid query")
        sys.exit(1)


# This functions sequentially executes regex patterns
def run(query,master_pp_dict):

    # Removes all unneccesaary characters from query string
    query = validate_query(query)
    # get user query and extract isin
    updated_isin_query, intent_check_query, master_pp_dict = isin_check(query,master_pp_dict)
    # get isin updated query and extract cusip
    updated_cusip_query, intent_check_query ,master_pp_dict = cusip_check(updated_isin_query, intent_check_query, master_pp_dict)
    # returns updated query and master_dict with extracted values
    updated_query, intent_check_query  = mark_flags(intent_check_query)
    return updated_query, intent_check_query, master_pp_dict

