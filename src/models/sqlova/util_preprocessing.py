"""
This utility declares some of the components for Preprocessing module
"""

from duckling import DucklingWrapper
duckling_wrapper = DucklingWrapper()


# Suffix word to be appended for cusip codes
cusip_suffix = " as cusip"

# Suffix word to be appended for ISIN codes
isin_suffix = " as ISIN"

# Suffix word to be appended for buy_sell flag
buy_sell_suffix = " buy_sell_flag"

# Suffix word to be appended for normal_status flag
nrml_status_suffix = " status status"
fail_status_suffix = " status"

# Regex pattern for CUSIP CODE
cusip_code_regex = r'(?=.*?[a-zA-Z])(?=[a-zA-Z]*?[0-9])[\w]{9,9}'

# Regex pattern for ISIN_CODE
isin_code_regex = r'(?=.*?[a-zA-Z])(?=[a-zA-Z]*?[0-9])[\w]{12,12}'


# Additional Flags
# We are currently handling following flags
# NRML_STATUS columns excecpt settled/SETTLED
buy_sell = ['buy','sell']
nrml_status = ['pending', 'matched', 'cancelled', 'unmatched', 'open', 'dk', 'uninstructed', 'instructed','unsettled']
fail_status = ['fail', 'failed']
fail_indicator_values = ['y','n']
