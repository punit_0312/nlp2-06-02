import util_preprocessing
import datetime
from dateutil.relativedelta import *

class DateProcess():

    def extract_dates(self,query):
        try:
            raw_date_results = util_preprocessing.duckling_wrapper.parse_time(query)
        except:
            raw_date_results = False
        return raw_date_results


    def validate_date(self,extracted_date):

        year = int(extracted_date.split('-')[0])
        if year in range(2010,2030):
            return extracted_date
        else:
            return False

    def dates_processing(self,query):

        raw_date_results = self.extract_dates(query)
        temp_query = [query]
        extracted_date = []

        if raw_date_results:
            for i in range(0, len(raw_date_results)):
                date_text = raw_date_results[i]['text']

                # Checks for time period if available in result
                try:
                    grain = raw_date_results[i]['value']['grain']
                except:
                    grain = False

                # This will extract date in case of only date value is available in string when user is asking for range till X
                # EXample :- Trades for last 2 weeks
                try:
                    to_date = self.validate_date(raw_date_results[i]['value']['value']['to'].split("T")[0])
                except:
                    to_date = False

                # This will extract date in case of only date value is available in string when user is asking for range starting from X
                # EXample :- Trades for next 2 weeks
                try:
                    from_date = self.validate_date(raw_date_results[i]['value']['value']['from'].split("T")[0])
                except:
                    from_date = False

                # This will extract date in case of only date value is available in string
                try:
                    date_value = self.validate_date(raw_date_results[i]['value']['value'].split("T")[0])
                except:
                    date_value = False

                if grain == 'day' and date_value:
                    temp_query.append(str(temp_query[-1]).replace(date_text, date_value))
                    extracted_date.append(date_value)

                elif grain == 'year' and date_value:
                    temp_query.append(str(temp_query[-1]).replace(date_text, date_value))
                    extracted_date.append(date_value)

                elif grain == 'month' and date_value:
                    month_end_date = datetime.datetime.strptime(date_value, "%Y-%m-%d") + relativedelta(months=+1)
                    temp_query.append(str(temp_query[-1]).replace(date_text, (date_value + " and " + str(month_end_date).split(" ")[0])))
                    extracted_date.append(date_value + " and " + str(month_end_date).split(" ")[0])

                elif grain == 'week' and date_value:
                    week_end_date = datetime.datetime.strptime(date_value, "%Y-%m-%d") + relativedelta(weeks=1)
                    temp_query.append(str(temp_query[-1]).replace(date_text, (date_value + " and " + str(week_end_date).split(" ")[0])))
                    extracted_date.append(date_value + " and " + str(week_end_date).split(" ")[0])

                elif from_date and to_date:
                    temp_query.append(str(temp_query[-1]).replace(date_text,(from_date + " and " + to_date)))
                    extracted_date.append(from_date + " and " + to_date)

                elif from_date:
                    temp_query.append(str(temp_query[-1]).replace(date_text,from_date))
                    extracted_date.append(from_date)

                elif to_date:
                    temp_query.append(str(temp_query[-1]).replace(date_text,to_date))
                    extracted_date.append(to_date)

                else:
                    temp_query.append(str(temp_query[-1]).replace(date_text, date_text))
                    extracted_date.append("Empty")
            return temp_query[-1], extracted_date

        else:
            extracted_date.append("Empty")
            return query,extracted_date


