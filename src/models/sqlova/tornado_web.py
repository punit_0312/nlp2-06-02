from tornado.wsgi import WSGIContainer
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from app import app

import config
import tornado.options
 
tornado.options.options.log_file_prefix = config.access_log_file_name
tornado.options.options.log_file_max_size = 10 * 1024 * 1024
tornado.options.parse_command_line()
 
http_server = HTTPServer(WSGIContainer(app))
http_server.listen(5000)
IOLoop.instance().start()