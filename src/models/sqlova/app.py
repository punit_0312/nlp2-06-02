"""
This is application file for FLask
Author :- Punit Shah
Date :- 02/01/2020
"""
import logging
import run
import config

from flask import Flask, request, jsonify, make_response
from datetime import datetime
from logging.handlers import RotatingFileHandler
from json import dumps
from query_classification.multi_query_classifier import Query_Classification

obj_query_classifier = Query_Classification()
print(type(obj_query_classifier))

app = Flask(__name__)

# This is landing API for sqlova operations. Accepts string as user query.
@app.route('/sqlova', methods=['POST'])
def search():
    """
    This functions accepts string as user query and starts operations for sqlova.
    """
    req_data = request.get_json()
    if 'query' in req_data:
        # This is empty dict which gets created for every request. It's returns back to client with values for concerned elements.
        master_dict = {'TRD_DT':[], 'CON_SETL_DT':[], 'ACCT_NM':[], 'CLNT_MNEM': [], 'CUSIP_CODE':[], 'ISIN_CODE':[], 'NRML_STATUS':[], 'QTY_REMN':[],'FAIL_IND':[], 'BUY_SELL':[]}
        raw_query = req_data['query']
        # Lets get the actual user query after extracting request_id and soe_id
        if "||" in raw_query:
            query = raw_query.split("||")[-1]
            conversation_id = raw_query.split("||")[0]
            user_id = raw_query.split("||")[1]
        else:
            query = raw_query
            conversation_id = None
            user_id = None
        
        # Lets check the type of the query
        query_type = obj_query_classifier.run(str(query))
        print("Query Type Label is :- ")
        print(query_type)
        
        # We are calling run function from run file, which takes care of pre-processing, regex operations, SQlova & Validation on user query.
        output = run.run(query,master_dict,conversation_id,user_id)
        # log user query along with its result for future debugging and tracking
        with open(config.queries_log_file_name, 'a') as file:
            file.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " --- " + req_data['query'] + " --- " + str(output) + "\n")
    else:
        # returns valid user message
        output = "Kindly pass query argument with user query"
    return make_response(dumps(output))

# This is landing API for sqlova operations. Accepts string as user query.
@app.route('/', methods=['GET'])
def home():
    """
    This functions accepts string as user query and starts operations for sqlova.
    """
    return 'Welcome to Sqlova!!!'