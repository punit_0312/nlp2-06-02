import config
import run
from class_date_processing import DateProcess
dt_parser_obj = DateProcess()


def executeGraph(value, relation, columnName):
    query = '''
            match ()-{}->(m:column)
            where {} in m.value and m.name = {}
            return m.name as name, {} as value
            '''.format(relation, value, columnName, value)
    graphResults = config.graph.run(query)
    return graphResults.data()


def executeAccGraph(value, relation, columnName):
    query = '''
            match ()-{}->(m:column)
            where m.name = {}
            unwind m.value as value
            with m,value
            where value contains {}
            return m.name as name, value
            '''.format(relation, columnName, value)
    graphResults = config.graph.run(query)
    return graphResults.data()


def outputFormatter(graphResults, response):
    key_name = graphResults[0].get('name')
    value = set([x.get('value') for x in graphResults])

    if key_name in response.keys() and value not in response[key_name]:
        response[key_name] = response[key_name].union(value)
    elif key_name not in response.keys():
        value_list = set()
        value_list = value_list.union(value)
        response[key_name] = value_list
    return response


def getRelation(columnName):
    relation_dict = {'[:FOR]': ["CLNT_MNEM", "ACCT_NM", "ISIN_CODE", "CUSIP_CODE"],
                     '[:HAS_TAG]': ["NRML_STATUS", "BUY_SELL", "QTY_REMN", "FAIL_IND"]}
    for key, value in relation_dict.items():
        if columnName in value:
            return key


def getColumnName(columnName, example_doc, response):
    value = ''
    relation = ''
    graphResults = ''
    relation = getRelation(columnName)
    columnName = "\'" + columnName + "\'"
    for tok in example_doc:
        value = "\'" + tok.text + "\'"
        value = value.upper()
        relation = relation
        if columnName != "'ACCT_NM'":
            if relation: graphResults = executeGraph(value, relation, columnName)
        else:
            if relation: graphResults = executeAccGraph(value, relation, columnName)
        if graphResults: response = outputFormatter(graphResults, response)
    return response


def process_dates(input_query,response,matched_intent,master_dict,extracted_date):

    if len(matched_intent) > 1:        
        for mi in matched_intent:
            val = " ".join(master_dict.get(mi)).replace("= ","")
            if val == '':
                available_value = " ".join(" ".join(master_dict.get(x)) for x in config.date_column_list if x in master_dict).replace("= ","").strip()
                for ed in reversed(extracted_date):
                    if ed != "Empty" and ed != available_value:
                        response[mi] = [ ed ]
                        extracted_date.remove(ed)
                        break
                    if ed == "Empty" or ed == available_value:
                        pass
            else:
                pass
    else:
        mi = matched_intent[0]
        val = " ".join(master_dict.get(mi)).replace("= ","")
        if val == '':
            available_value = " ".join(" ".join(master_dict.get(x)) for x in config.date_column_list if x in master_dict).replace("= ","").strip()
            if available_value == '':
                try:
                    extracted_date = list(filter(lambda x: x!= 'Empty', extracted_date))
                except:
                    pass

                if len(extracted_date) > 1:
                    response[mi] = [" AND ".join(extracted_date)]
                else:
                    response[mi] = extracted_date
            else:
                for ed in extracted_date:    
                    if ed != "Empty" and ed != available_value:
                        response[mi] = [ ed ]
                    if ed == "Empty" or ed == available_value:
                        pass
        else:
            pass
    return response

def getCol(column_list, input_query,master_dict,extracted_date):

    response = {}
    matched_intent = [x for x in config.date_column_list if x in column_list]
    quant_col = [x for x in config.quantitative_column_list if x in column_list]

    if matched_intent:
        response = process_dates(input_query,response,matched_intent,master_dict,extracted_date)
    # As of now, we are assuming only one column in this kind, later on we can change it
    if quant_col:
        run.post_processing(quant_col[0],master_dict,input_query)


    example_doc = config.nlp(input_query)
    results = ''
    for i in column_list:
        results = getColumnName(i, example_doc, response)
        if i in results.keys():
            if i in 'ACCT_NM':
                results[i] = [x for x in results.get(i) if x.lower() in input_query.lower()]
            elif i in 'FAIL_IND':
                results[i] = ['Y']
            else:
                results[i] = list(results[i])
    return results