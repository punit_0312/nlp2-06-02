import numpy as np
import keras
import tensorflow as tf
import tensorflow_hub as hub
import config
from keras.models import Model
from keras.layers import Input
from keras.layers.core import Dense, Lambda
import os
from keras import backend as K
from tensorflow import Graph,Session

class Multi_Intent():

    def __init__(self):
        self.model = self.build_model()

    def ELMoEmbedding(self,x):
        embed = hub.Module("intent_classification//9bb74bc86f9caffc8c47dd7b33ec4bb354d9602d.95ac68fb562341b39b1fb7efde7d68a8")
        # return embed(tf.squeeze(tf.cast(x, tf.string)), signature="default", as_dict=True)["default"]
        return embed(tf.reshape(tf.cast(x, tf.string), [-1]), signature="default", as_dict=True)["default"]

    def build_model(self):

        global graph
        global session
        with tf.Graph().as_default() as graph:
            session = Session()
            with session.as_default():
                input_text = Input(shape=(1,), dtype="string")
                embedding = Lambda(self.ELMoEmbedding, output_shape=(1024,),)(input_text)
                dense1 = Dense(256, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001))(embedding)
                dense2 = Dense(100, activation='relu', kernel_regularizer=keras.regularizers.l2(0.001))(dense1)
                pred = Dense(10, activation='sigmoid')(dense2)
                model = Model(inputs=[input_text], outputs=pred)
                #model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
                model.load_weights("intent_classification\\full_model_elmo_weights.h5")
                return model

    def pred(self,arr):

        with graph.as_default():
            with session.as_default():
                preds = self.model.predict(arr)
        
        pred_labels = (preds > 0.85).astype(np.int)
        pred_label_list = pred_labels.tolist()

        column = config.master_list_columns
        intent_list = []
        for index, pl in enumerate(pred_label_list[0]):
            if pl == 1:
                intent_list.append(column[index])
        return intent_list

    def run(self,query):
        input_str = query
        input_arr = np.array([input_str])
        extracted_intents = self.pred(input_arr)
        return list(extracted_intents)