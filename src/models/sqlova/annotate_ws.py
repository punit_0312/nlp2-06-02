
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
import os
import config
import ujson as json
from stanza.nlp.corenlp import CoreNLPClient
from tqdm import tqdm
import copy
from wikisql.lib.common import count_lines, detokenize
from wikisql.lib.query import Query


client = None

class Annotate_WS():
    def __init__(self):
        pass

    def annotate(self,sentence, lower=True):
        global client
        if client is None:
            client = CoreNLPClient(default_annotators='ssplit,tokenize'.split(','))
        words, gloss, after = [], [], []
        for s in client.annotate(sentence):
            for t in s:
                words.append(t.word)
                gloss.append(t.originalText)
                after.append(t.after)
        if lower:
            words = [w.lower() for w in words]
        return {
            'gloss': gloss,
            'words': words,
            'after': after,
        }

    def annotate_example(self,example, table):
        ann = {'table_id': example['table_id']}
        ann['question'] = self.annotate(example['question'])
        ann['table'] = {
            'header': [self.annotate(h) for h in table['header']],
        }
        ann['query'] = sql = copy.deepcopy(example['sql'])
        for c in ann['query']['conds']:
            c[-1] = self.annotate(str(c[-1]))

        q1 = 'SYMSELECT SYMAGG {} SYMCOL {}'.format(Query.agg_ops[sql['agg']], table['header'][sql['sel']])
        q2 = ['SYMCOL {} SYMOP {} SYMCOND {}'.format(table['header'][col], Query.cond_ops[op], detokenize(cond)) for
              col, op, cond in sql['conds']]
        if q2:
            q2 = 'SYMWHERE ' + ' SYMAND '.join(q2) + ' SYMEND'
        else:
            q2 = 'SYMEND'
        inp = 'SYMSYMS {syms} SYMAGGOPS {aggops} SYMCONDOPS {condops} SYMTABLE {table} SYMQUESTION {question} SYMEND'.format(
            syms=' '.join(['SYM' + s for s in Query.syms]),
            table=' '.join(['SYMCOL ' + s for s in table['header']]),
            question=example['question'],
            aggops=' '.join([s for s in Query.agg_ops]),
            condops=' '.join([s for s in Query.cond_ops]),
        )
        ann['seq_input'] = self.annotate(inp)
        out = '{q1} {q2}'.format(q1=q1, q2=q2) if q2 else q1
        ann['seq_output'] = self.annotate(out)
        ann['where_output'] = self.annotate(q2)
        assert 'symend' in ann['seq_output']['words']
        assert 'symend' in ann['where_output']['words']
        return ann

    def find_sub_list(self,sl, l):
        # from stack overflow.
        results = []
        sll = len(sl)
        for ind in (i for i, e in enumerate(l) if e == sl[0]):
            if l[ind:ind + sll] == sl:
                results.append((ind, ind + sll - 1))

        return results

    def check_wv_tok_in_nlu_tok(self,wv_tok1, nlu_t1):
        """
        Jan.2019: Wonseok
        Generate SQuAD style start and end index of wv in nlu. Index is for of after WordPiece tokenization.

        Assumption: where_str always presents in the nlu.

        return:
        st_idx of where-value string token in nlu under CoreNLP tokenization scheme.
        """
        g_wvi1_corenlp = []
        nlu_t1_low = [tok.lower() for tok in nlu_t1]
        for i_wn, wv_tok11 in enumerate(wv_tok1):
            wv_tok11_low = [tok.lower() for tok in wv_tok11]
            results = self.find_sub_list(wv_tok11_low, nlu_t1_low)
            st_idx, ed_idx = results[0]

            g_wvi1_corenlp.append([st_idx, ed_idx])

        return g_wvi1_corenlp

    def annotate_example_ws(self,example, table):
        """
        Jan. 2019: Wonseok
        Annotate only the information that will be used in our model.
        """
        ann = {'table_id': example['table_id'], 'phase': example['phase']}
        _nlu_ann = self.annotate(example['question'])
        ann['question'] = example['question']
        ann['question_tok'] = _nlu_ann['gloss']
        # ann['table'] = {
        #     'header': [annotate(h) for h in table['header']],
        # }
        ann['sql'] = example['sql']
        ann['query'] = sql = copy.deepcopy(example['sql'])

        conds1 = ann['sql']['conds']
        wv_ann1 = []
        for conds11 in conds1:
            _wv_ann1 = self.annotate(str(conds11[2]))
            wv_ann11 = _wv_ann1['gloss']
            wv_ann1.append(wv_ann11)

            # Check whether wv_ann exsits inside question_tok

        try:
            wvi1_corenlp = self.check_wv_tok_in_nlu_tok(wv_ann1, ann['question_tok'])
            ann['wvi_corenlp'] = wvi1_corenlp
        except:
            ann['wvi_corenlp'] = None
            ann['tok_error'] = 'SQuAD style st, ed are not found under CoreNLP.'

        return ann

    def is_valid_example(self,e):
        if not all([h['words'] for h in e['table']['header']]):
            return False
        headers = [detokenize(h).lower() for h in e['table']['header']]
        if len(headers) != len(set(headers)):
            return False
        input_vocab = set(e['seq_input']['words'])
        for w in e['seq_output']['words']:
            if w not in input_vocab:
                print('query word "{}" is not in input vocabulary.\n{}'.format(w, e['seq_input']['words']))
                return False
        input_vocab = set(e['question']['words'])
        for col, op, cond in e['query']['conds']:
            for w in cond['words']:
                if w not in input_vocab:
                    print('cond word "{}" is not in input vocabulary.\n{}'.format(w, e['question']['words']))
                    return False
        return True

    def main(self,request_id):
        parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
        parser.add_argument('--din', default=config.IN_support_directoy, help='data directory')
        parser.add_argument('--dout', default=config.OUT_support_directoy, help='output directory')
        _temp_split = 'citi'
        args = parser.parse_args()

        answer_toy = not True
        toy_size = 10

        if not os.path.isdir(args.dout):
            os.makedirs(args.dout)

        # for split in ['train', 'dev', 'test']:
        for split in request_id.split(','):
            fsplit = os.path.join(args.din, split) + '.jsonl'
            ftable = os.path.join(args.din, _temp_split) + '.tables.jsonl'
            fout = os.path.join(args.dout, split) + '_tok.jsonl'

            with open(fsplit) as fs, open(ftable) as ft, open(fout, 'wt') as fo:
                # ws: Construct table dict with table_id as a key.
                tables = {}
                for line in tqdm(ft, total=count_lines(ftable)):
                    d = json.loads(line)
                    tables[d['id']] = d
                n_written = 0
                cnt = -1
                for line in tqdm(fs, total=count_lines(fsplit)):
                    cnt += 1
                    d = json.loads(line)
                    # a = self.annotate_example(d, tables[d['table_id']])
                    a = self.annotate_example_ws(d, tables[d['table_id']])
                    fo.write(json.dumps(a) + '\n')
                    n_written += 1

                    if answer_toy:
                        if cnt > toy_size:
                            break