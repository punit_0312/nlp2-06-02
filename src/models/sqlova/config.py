"""
This config file provides essential parameters for running SQLova.
Author :- Punit Shah
Date :- 14th Jan 2020
"""

import os
from py2neo import Graph
import spacy

# Below parameters are used for sqlova Seq2SQL operations
table_id = 'citi'
split_name = 'citi'

# Below parameters are used for sqlova BERT operations
temp_file_name = 'citi.csv'
temp_data_path = "data"
IN_support_directoy = "user_query_files"
OUT_support_directoy = "user_query_files"
model_bert_path = "pretrained\\model_bert_best.pt"
model_path = "pretrained\\model_best.pt"
bert_config_path = "support"
output_path = "output"
intent_model_file_name = "full_model_elmo_weights.h5"


# Below parameter is used for setting up logs locations
error_log_path = "logs"
intent_log_path = "logs"
sqlova_log_path = "logs"

# Below parameters are used in setting up error & access log file names
error_file_name = os.path.join(error_log_path,'error.log')
access_log_file_name = os.path.join(error_log_path,'access.log')
queries_log_file_name = os.path.join(error_log_path,'queries.txt')

# Below parameters are used in setting up log file names for intent_classification & SQLova
intent_result_file_name = os.path.join(intent_log_path,'intents_output.log')
sqlova_result_file_name = os.path.join(sqlova_log_path,'sqlova_output.log')

# Below is the master mapping dict, for which key is column name used in sample table of sqlova and value is actual filter name in citi
# Below mapper is used to transform sqlova output in desired format.
master_mapper_dict = { 'Trade_date':'TRD_DT',
                       'Settlement_Date/Trade_Settlement_Date/Settled/Settling':'CON_SETL_DT',
                       'Account_Name':'ACCT_NM',
                       'Client_Mnemonics' : 'CLNT_MNEM',
                       'Status/Trade_Status/status_flag': 'NRML_STATUS',
                       'quantity/Trade_quantity': 'QTY_REMN',
                       'Failed/Fail/failing' : 'FAIL_IND',
                       'Buy_Flag/Sell_Flag/buy_sell_flag/buy/sell' : 'BUY_SELL'
}

# Below is the master list of columns that our module is supporting for now.
# Below is used in validation module.
master_list_columns = ["CLNT_MNEM", "CON_SETL_DT", "ACCT_NM", "ISIN_CODE", "TRD_DT", "CUSIP_CODE", "NRML_STATUS", "BUY_SELL", "QTY_REMN","FAIL_IND"]

# Graph configurations
nlp = spacy.load('en_core_web_sm')
graph = Graph("bolt://localhost:7687", auth=("test", "test"))
tx = graph.begin()
relation_dict = {'[:FOR]': ["CLNT_MNEM","ACCT_NM","ISIN_CODE","CUSIP_CODE"], '[:HAS_TAG]': ["NRML_STATUS", "BUY_SELL", "QTY_REMN","FAIL_IND"]}

# Columns grouping
date_column_list = ["CON_SETL_DT","TRD_DT"]
categorical_column_list = ["CLNT_MNEM","ACCT_NM", "ISIN_CODE","CUSIP_CODE", "NRML_STATUS", "BUY_SELL","FAIL_IND"]
quantitative_column_list = ["QTY_REMN"]




