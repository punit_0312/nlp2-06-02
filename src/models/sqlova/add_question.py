import json
import config
import os


def question_to_json(split,table_id, question):
    json_file_name = '{}.jsonl'.format(split)
    record = {
        'phase': 1,
        'table_id': table_id,
        'question': question,
        'sql': {'sel': 0, 'conds': [], 'agg': 0}
    }
    file_path = config.IN_support_directoy
    with open(os.path.join(file_path,json_file_name), 'a+') as fout:
        json.dump(record, fout)
        fout.write('\n')
