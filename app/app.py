"""
This is application file for FLask
Author :- Punit Shah
Date :- 02/01/2020
"""

from flask import Flask, request, jsonify
from datetime import timedelta, datetime
from logging.handlers import RotatingFileHandler

import logging
import run
import config

app = Flask(__name__)

# This is landing API for sqlova operations. Accepts string as user query.
@app.route('/sqlova', methods=['POST'])
def search():
    """
    This functions accepts string as user query and starts operations for sqlova.
    """
    req_data = request.get_json()
    if 'query' in req_data:
        # This is empty dict which gets created for every request. It's returns back to client with values for concerned elements.
        master_dict = {'TRD_DT':[], 'CON_SETL_DT':[], 'ACCT_NM':[], 'CLNT_MNEM': [], 'CUSIP_CODE':[], 'ISIN_CODE':[], 'NRML_STATUS':[], 'QTY_REMN':[],'FAIL_IND':[], 'BUY_SELL':[]}
        query = req_data['query']
        # We are calling run function from run file, which takes care of pre-processing, regex operations, SQlova & Validation on user query.
        output = run.run(query,master_dict)
        # log user query along with its result for future debugging and tracking
        with open(config.queries_log_file_name, 'a') as file:
            file.write(datetime.now().strftime('%Y-%m-%d %H:%M:%S') + " --- " + req_data['query'] + " --- " + output + "\n")
    else:
        # returns valid user message
        output = "Kindly pass query argument with user query"
    return jsonify({'output': output})


if __name__ == '__main__':
    logging.basicConfig(filename=config.error_file_name, level=logging.ERROR)
    handler = RotatingFileHandler(filename=config.access_log_file_name, maxBytes=10000, backupCount=1)
    logger = logging.getLogger('werkzeug')
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)
    app.run(host='0.0.0.0', debug=True)