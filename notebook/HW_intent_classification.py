import pandas as pd
import numpy as np
import json
import nltk
import re
import csv
import matplotlib.pyplot as plt
import seaborn as sns
from tqdm import tqdm
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split



commnt_df = pd.read_excel(r"input_text_V1.xlsx", sheet_name="input_text")
print (list(commnt_df))
commnt_df['target_intents'] = commnt_df['Actual_Intents'].str.lower().apply(lambda x: x.split(','))
print (commnt_df['target_intents'])

# get all intent tags in a list
all_intents = [w.strip() for temp in commnt_df['target_intents'].tolist() for w in temp]
print (len(set(all_intents)))


all_intents = nltk.FreqDist(all_intents)

# create dataframe
all_intents_df = pd.DataFrame({'Intent': list(all_intents.keys()),
                              'Count': list(all_intents.values())})

g = all_intents_df.nlargest(columns="Count", n = 50)
plt.figure(figsize=(12,15))
ax = sns.barplot(data=g, x= "Count", y = "Intent")
ax.set(ylabel = 'Count')
# plt.show()


# function for text cleaning
def clean_text(text):
    # remove backslash-apostrophe
    text = re.sub("\'", "", text)
    # remove everything except alphabets
    text = re.sub("[^a-zA-Z0-9]", " ", text)
    # remove whitespaces
    text = ' '.join(text.split())
    # convert text to lowercase
    text = text.lower()
    return text

commnt_df['clean_text'] = commnt_df['Search Query'].apply(lambda x: clean_text(x))


def freq_words(x, terms=30):
    all_words = ' '.join([text for text in x])
    all_words = all_words.split()
    fdist = nltk.FreqDist(all_words)
    words_df = pd.DataFrame({'word': list(fdist.keys()), 'count': list(fdist.values())})

    # selecting top 20 most frequent words
    d = words_df.nlargest(columns="count", n=terms)

    # visualize words and frequencies
    plt.figure(figsize=(12, 15))
    ax = sns.barplot(data=d, x="count", y="word")
    ax.set(ylabel='Word')
    # plt.show()


# print 100 most frequent words
freq_words(commnt_df['clean_text'], 100)

nltk.download('stopwords')
from nltk.corpus import stopwords
stop_words = set(stopwords.words('english'))

# function to remove stopwords
def remove_stopwords(text):
    no_stopword_text = [w for w in text.split() if not w in stop_words]
    return ' '.join(no_stopword_text)

commnt_df['clean_text'] = commnt_df['clean_text'].apply(lambda x: remove_stopwords(x))
# print 100 most frequent words
freq_words(commnt_df['clean_text'], 100)


from sklearn.preprocessing import MultiLabelBinarizer

multilabel_binarizer = MultiLabelBinarizer()
multilabel_binarizer.fit(commnt_df['target_intents'])

# transform target variable
y = multilabel_binarizer.transform(commnt_df['target_intents'])


# split dataset into training and validation set
xtrain_sample, xval_sample, ytrain, yval = train_test_split(commnt_df, y, test_size=0.2, random_state=9)
print ("@@@@@@@@@@@@@@@@Training data@@@@@@@@@@@@@@@@@@@")
print (list(xtrain_sample))
print (xtrain_sample.shape)
print (xtrain_sample['clean_text'].shape)
print (len(ytrain))
print ("@@@@@@@@@@@@@@@@Testing data@@@@@@@@@@@@@@@@@@@")
print (list(xval_sample))
print (xval_sample.shape)
print (xval_sample['clean_text'].shape)
print (len(yval))

#########To create tfidf vectors
tfidf_vectorizer = TfidfVectorizer(max_df=0.8, max_features=10000)
# create TF-IDF features
xtrain = xtrain_sample['clean_text']
xval = xval_sample['clean_text']
xtrain_tfidf = tfidf_vectorizer.fit_transform(xtrain)
xval_tfidf = tfidf_vectorizer.transform(xval)



from sklearn.linear_model import LogisticRegression

# Binary Relevance
from sklearn.multiclass import OneVsRestClassifier

# Performance metric
from sklearn.metrics import f1_score

lr = LogisticRegression()
clf = OneVsRestClassifier(lr)
# fit model on train data
clf.fit(xtrain_tfidf, ytrain)

# make predictions for validation set
y_pred = clf.predict(xval_tfidf)
print (len(multilabel_binarizer.inverse_transform(y_pred)))


# evaluate performance
print (f1_score(yval, y_pred, average="micro"))


# predict probabilities
y_pred_prob = clf.predict_proba(xval_tfidf)


t = 0.388 # threshold value
y_pred_new = (y_pred_prob >= t).astype(int)

# evaluate performance
print (f1_score(yval, y_pred_new, average="micro"))
print (multilabel_binarizer.inverse_transform(y_pred_new)[13])


sample_out = []
columns = (list(xval_sample))
columns.append('predicted_res')
count = 0
for index, row in xval_sample.iterrows():
    each_row = list(row)
    pred_result = list(set(multilabel_binarizer.inverse_transform(y_pred_new)[count]))
    each_row.append(pred_result)
    sample_out.append(each_row)
    count += 1
pred_df = pd.DataFrame(sample_out, columns=columns)
print(pred_df)
pred_df.to_exce(r'Output_pred.xlsx', index=False)