from py2neo import Graph

graph = Graph("bolt://localhost:11002", auth=("neo4j", "test")) 	# configuration for neo4j
tx = graph.begin()
    
status = ['PENDING', 'MATCHED', 'CANCELLED', 'UNMATCHED', 'OPEN', 'DK', 'UNINSTRUCTED', 'INSTRUCTED', 'SETTLED','UNSETTLED']
fail_ind = ['FAILED','FAIL','FAILING']
buy_sell = ['BUY','SELL']
with open('CLNT_MNEM.txt', 'r') as f:
    clnt_mnem = f.read().splitlines()
with open('ACCT_NM.txt', 'r') as f:
    acct_nm = f.read().splitlines()
node_dict = {'trades': ['Trades'], 'NRML_STATUS': status, 'FAIL_IND':fail_ind, 'BUY_SELL':buy_sell, 'CLNT_MNEM':clnt_mnem, 'ACCT_NM': acct_nm}
relation_dict = {'CLNT_MNEM': '[:FOR]', 'ACCT_NM':'[:FOR]', 'NRML_STATUS':'[:HAS_TAG]', 'FAIL_IND':'[:HAS_TAG]', 'BUY_SELL':'[:HAS_TAG]'}

def deleteGraph():
    query = "MATCH (n) DETACH DELETE n"
    graphResults = graph.run(query)

def createNodes(key,value):
    query = "CREATE (:column {{ name:{}, value:{} }})".format(key, value)
    graphResults = graph.run(query)

def createRelation(key, value):
    query = '''MATCH (trades:column),(cl:column)
             WHERE trades.name = 'trades' AND cl.name = {}
             CREATE (trades)-{}->(cl)'''.format(key, value)
    graphResults = graph.run(query)


if __name__ == "__main__":
    deleteGraph()                   #delete if a graph already exists exists
    for key,value in node_dict.items():
        key = '\"' + key + '\"'
        createNodes(key,value)
    for key,value in relation_dict.items():
        key = '\"' + key + '\"'
        createRelation(key,value)
