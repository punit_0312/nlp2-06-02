#!/bin/sh

# get present working dirname
current_dir=$(pwd)
echo "current_dir":"$current_dir"

# settling path variable
export PATH=/opt/middleware/anaconda_python/3.5.4/bin:$PATH 

# setting python path
export PYTHONPATH=${PYTHONPATH}:${current_dir}

function run_corenlp_model {
	echo "Start stanford corenlp process"
	echo "------------------------------------------------"
    corenlp_dir="${current_dir}/src/models/sqlova/corenlp"
    echo ${corenlp_dir}
    # get the process id for existing corenlp server
    PID=ps -ef | grep edu.stanford.nlp.pipeline.StanfordCoreNLPServer | grep -v grep | awk '{print $2}'
    if [[ "" !=  "${PID}" ]]; then
      kill -9 $PID
    fi

    cd ${corenlp_dir}/stanford-corenlp-full-2018-10-05  
    echo "Started core-nlp server, with pid: "
    PID=nohup java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer & echo $!
    echo "------------------------------------------------"
    if [[ "" !=  "${PID}" ]]; then
      # calling for executing sqlova flask engine
      run_sqlova_model()
    fi
}

function run_sqlova_model {
	echo "Start sqlova flask server.."
	echo "------------------------------------------------"
    support_dir="${current_dir}/src/models/sqlova/support"
    pretrained_dir="${current_dir}/src/models/sqlova/pretrained"
    corenlp_dir="${current_dir}/src/models/sqlova/corenlp"
    if [ -d "$support_dir" ] && [ -d $pretrained_dir ] && [ -d $corenlp_dir ] ; then
      echo "support files exist in ${support_dir}..."
      echo "pretrained files exist in ${pretrained_dir}..."
      echo "Stanford corenlp files exist in ${corenlp_dir}..."
      echo "started sqlova flask server, with pid: "
      echo "------------------------------------------------"
      nohup python ${current_dir}/src/models/sqlova/app.py & echo $!
      echo "------------------------------------------------"
    else
      echo "Error: Support, Pretrained and Stanford Corenlp files not added properly. Cannot continue."
      exit 1
    fi

}

# calling for executing stanford corenlp
run_corenlp_model()


