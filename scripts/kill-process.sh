#!/bin/sh

# get present working dirname
current_dir=$(pwd)
echo "current_dir":"$current_dir"

# settling path variable
export PATH=/opt/middleware/anaconda_python/3.5.4/bin:$PATH 

# setting python path
export PYTHONPATH=${PYTHONPATH}:${current_dir}

function kill_corenlp_model {
    corenlp_dir="${current_dir}/src/models/sqlova/corenlp"

    # get the process id for existing corenlp server
    PID=ps -ef | grep edu.stanford.nlp.pipeline.StanfordCoreNLPServer | grep -v grep | awk '{print $2}'
    if [[ "" !=  "$PID" ]]; then
      kill -9 $PID
    fi
    echo "Killed stanford corenlp server..."
    echo "Process id: ${PID}"
    echo "------------------------------------------------"

}

function kill_sqlova_model {
    PID=kill -9 $(lsof -t -i:5000)
    echo "Killed Sqlova flask server..."
    echo "Process id: ${PID}"
	  echo "------------------------------------------------"

}

# killing stanford corenlp process
kill_corenlp_model()
# killing stanford sqlova flask engine
kill_sqlova_model()
